<?php

class BranchAndBound
{
    const DEFAULT_MAX_MINUTES = 15;


    /** @var Path  */
    public $path;

    /** @var Path  */
    protected $solution;

    /** @var array  */
    protected $distanceMatrix;

    /** @var DateTime  */
    protected $startTime;

    /** @var int  */
    protected $maxMinutes;


    /**
     * BranchAndBound constructor.
     * @param Path $path
     * @param int $maxMinutes
     */
    public function __construct($path, $maxMinutes = self::DEFAULT_MAX_MINUTES)
    {
        $this->path = $path;
        $this->distanceMatrix = $path->getDistanceMatrix();
        $this->startTime = new DateTime();
        $this->maxMinutes = $maxMinutes;
    }

    /**
     * Branch and Bound algorithm
     * @return Path
     */
    function getMinPath() {
        $candidates = new Candidates();
        //Initial solution using optimal solution
        $this->solution = $this->getOptimalPath(new Path([$this->path->getFirstCity()], 0.0));
        $candidate = new Path([$this->path->getFirstCity()], 0.0, $this->solution->cost);
        $candidates->insert($candidate);

        while ($candidates->count() > 0) {
            if ($this->isMaxTime()) {
                return $this->solution;
            }

            $candidate = $candidates->next();
            //Pruning
            if ($candidate->cost >= $this->solution->cost) {
                continue;
            }

            $citiesNotVisited = $candidate->getDiff($this->path);
            foreach ($citiesNotVisited->cities as $city) {
                $candidateTmp = new Path($candidate->cities, $candidate->cost, $candidate->optimalCost);
                $candidateTmp->addCity($city);

                //It's not a good candidate
                if ($candidateTmp->cost > $this->solution->cost){
                    continue;
                }

                //We got a better solution
                if (($candidateTmp->count() === $this->path->count()) && ($candidateTmp->cost < $this->solution->cost)) {
                    $this->solution = $candidateTmp;
                } //It's a candidate
                else {
                    $optimalCandidate = $this->getOptimalPath($candidateTmp);
                    $candidateTmp->optimalCost = $optimalCandidate->optimalCost;
                    $candidates->insert($candidateTmp);

                    //If optimal solution for this path is better update solution
                    if ($optimalCandidate->cost < $this->solution->cost) {
                        $this->solution = $optimalCandidate;
                    }
                }
            }
        }

        return $this->solution;
    }

    /**
     * Algorithm to get an optimal solution for a partial solution
     * @param Path $solution  Partial solution
     * @return Path
     */
    protected function getOptimalPath($solution) {
        $result = new Path($solution->cities, $solution->cost);

        while ($result->count() < $this->path->count()) {
            $lastCandidateVisited = $result->getLastCity();
            $distanceVector = $this->distanceMatrix[$lastCandidateVisited->id - 1];

            //Starts from 1 because position 0 is distance with same city
            for ($i = 1; $i < count($distanceVector); $i++) {
                $distance = key($distanceVector[$i]);
                $city = $distanceVector[$i][$distance];

                if ($result->hasCity($city)) {
                    continue;
                }

                $result->addCity($city);
                break;
            }
        }

        $result->optimalCost = $result->cost;
        return $result;
    }

    /**
     * @return bool
     */
    protected function isMaxTime(){
        $currentMinutes = ($this->startTime->diff(new DateTime())->d * 24)
            + ($this->startTime->diff(new DateTime())->h * 60)
            + $this->startTime->diff(new DateTime())->i;

        if ($currentMinutes >= $this->maxMinutes) {
            return true;
        }

        return false;
    }
}
