<?php

class City{
    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var float */
    public $posX;

    /** @var float */
    public $posY;


    /**
     * city constructor.
     * @param int $id
     * @param string $name
     * @param float $posX
     * @param float $posY
     */
    public function __construct($id, $name, $posX, $posY)
    {
        $this->id = $id;
        $this->name = $name;
        $this->posX = $posX;
        $this->posY = $posY;
    }

    /**
     * @param City $city
     * @return float
     */
    public function getDistance($city) {
        return round(
            sqrt(pow($this->posX - $city->posX, 2) + pow($this->posY - $city->posY, 2)),
            2
        );
    }
}

