<?php

class TestPath extends Test
{
    public function testGetCity() {
        $cities1 = new Path([
            new City(1,'City 1', 40.21, 40.38),
            new City(2, 'City 2', 10.24, 10.35)
        ]);

        $expectedResult = new City(1,'City 1', 40.21, 40.38);
        $this->assertSameCities(new Path([$cities1->getCity(0)]), new Path([$expectedResult]));
    }

    public function testGetCityById() {
        $cities1 = new Path([
            new City(1,'City 1', 40.21, 40.38),
            new City(2, 'City 2', 10.24, 10.35)
        ]);

        $expectedResult = new City(2, 'City 2', 10.24, 10.35);
        $this->assertSameCities(new Path([$cities1->getCityById(2)]), new Path([$expectedResult]));
    }

    public function testGetFirstCity() {
        $cities1 = new Path([
            new City(1,'City 1', 40.21, 40.38),
            new City(2, 'City 2', 10.24, 10.35)
        ]);

        $expectedResult = new City(1,'City 1', 40.21, 40.38);
        $this->assertSameCities(new Path([$cities1->getFirstCity()]), new Path([$expectedResult]));
    }

    public function testGetLastCity() {
        $cities1 = new Path([
            new City(1,'City 1', 40.21, 40.38),
            new City(2, 'City 2', 10.24, 10.35)
        ]);

        $expectedResult = new City(2, 'City 2', 10.24, 10.35);
        $this->assertSameCities(new Path([$cities1->getLastCity()]), new Path([$expectedResult]));
    }

    public function testHasCity() {
        $cities = new Path([
            new City(1,'City 1', 40.21, 40.38),
            new City(2, 'City 2', 10.24, 10.35)
        ]);

        $city1 = new City(2, 'City 2', 10.24, 10.35);
        $city2 = new City(3, 'City 3', 34.24, 43.35);

        assert('$cities->hasCity($city1) === true', 'Invalid hasCity');
        assert('$cities->hasCity($city2) === false', 'Invalid hasCity');
    }

    public function testGetDiff(){
        $cities1 = new Path([
            new City(1,'City 1', 40.21, 40.38),
            new City(2, 'City 2', 10.24, 10.35)
        ]);

        $cities2 = new Path([
            new City(1,'City 1', 40.21, 40.38),
            new City(2, 'City 2', 10.24, 10.35),
            new City(3, 'City 3', 20.23, 20.36),
            new City(4, 'City 4', 30.22, 30.37)

        ]);

        $expectedResult = new Path([
            new City(3, 'City 3', 20.23, 20.36),
            new City(4, 'City 4', 30.22, 30.37)
        ]);

        $result = $cities1->getDiff($cities2);
        $empty1 = $cities2->getDiff($cities1);
        $empty2 = $cities2->getDiff($cities2);

        $this->assertSameCities($result, $expectedResult);
        $this->assertSameCities($empty1, []);
        $this->assertSameCities($empty2, []);
    }

    public function testAddCities() {
        $cities1 = new Path([
            new City(1,'City 1', 40.21, 40.38),
            new City(2, 'City 2', 10.24, 10.35)
        ]);

        $cities2 = new Path([
            new City(3, 'City 3', 20.23, 20.36),
            new City(4, 'City 4', 30.22, 30.37)
        ]);

        $expectedResult = new Path([
            new City(1,'City 1', 40.21, 40.38),
            new City(2, 'City 2', 10.24, 10.35),
            new City(3, 'City 3', 20.23, 20.36),
            new City(4, 'City 4', 30.22, 30.37)
        ]);

        $cities1->addCities($cities2);
        $this->assertSameCities($cities1, $expectedResult);

        $cities1->addCities($cities1);
        $this->assertSameCities($cities1, $cities1);

        $cities1->addCities(new Path([]));
        $this->assertSameCities($cities1, $cities1);
    }
}
