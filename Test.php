<?php

abstract class Test
{
    public function run(){
        $methods = get_class_methods($this);
        foreach ($methods as $method){
            if (strpos($method, 'test') !== false){
                echo "Testing  " . $method . "\n";
                $this->$method();
            }
        }
    }

    protected function assertSameCities($cities1, $cities2) {
        foreach ($cities1->cities as $key => $city) {
            assert('$city->id == $cities2->getCityById($city->id)->id', "Invalid city");
            assert('$city->name == $cities2->getCityById($city->id)->name', "Invalid city");
            assert('$city->posX == $cities2->getCityById($city->id)->posX', "Invalid city");
            assert('$city->posY == $cities2->getCityById($city->id)->posY', "Invalid city");
        }
    }
}


assert_options(ASSERT_ACTIVE, 1);
assert_options(ASSERT_WARNING, 0);
assert_options(ASSERT_QUIET_EVAL, 1);
assert_options(ASSERT_CALLBACK, 'assert_handler');

function assert_handler($file, $line, $code, $desc = null)
{
    echo "Assertion failed at $file:$line: $code";
    if ($desc) {
        echo ": $desc";
    }
    echo "\n";
}
