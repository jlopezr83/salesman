<?php

class Path
{
    /** @var City[] */
    public $cities;

    /**
     * Current cost
     * @var float
     */
    public $cost;

    /**
     * Optimal cost calculated
     * @var float
     */
    public $optimalCost;

    /**
     * cities constructor.
     * @param city[] $cities
     * @param float $cost
     * @param float $optimalCost
     */
    public function __construct($cities = [], $cost = null, $optimalCost = null)
    {
        $this->cities = [];
        foreach ($cities as $city) {
           $this->cities[] = new City($city->id, $city->name, $city->posX, $city->posY);
        }

        $this->cost = is_null($cost) ? $this->getTotalDistance() : $cost;
        $this->optimalCost = $optimalCost;
    }

    /**
     * @param $key
     * @return City
     */
    public function getCity($key) {
        return $this->cities[$key];
    }

    /**
     * @param $id
     * @return City
     */
    public function getCityById($id) {
        foreach ($this->cities as $city) {
            if ($city->id == $id) {
                return $city;
            }
        }

        return null;
    }

    /**
     * @return City
     */
    public function getFirstCity(){
        return $this->cities[0];
    }

    /**
     * @return City
     */
    public function getLastCity(){
        return $this->cities[$this->count() -1];
    }

    /**
     * @param Path $cities
     */
    public function addCities($cities) {
        foreach ($cities->cities as $city) {
            $lastVisited = empty($this->cities) ? null : $this->getLastCity();
            $this->cities[] = new City($city->id, $city->name, $city->posX, $city->posY);
            $this->cost += is_null($lastVisited) ? 0.0 : $lastVisited->getDistance($city);
        }
    }

    /**
     * @param City $city
     */
    public function addCity($city) {
        $this->addCities(new Path([$city]));
    }

    /**
     * @param City $city
     * @return bool
     */
    public function hasCity($city){
        if ($this->getCityById($city->id)) {
            return true;
        }

        return false;
    }

    /**
     * @param Path $cities
     * @return Path
     */
    public function getDiff($cities) {
        $result = [];
        $cost = 0;
        foreach ($cities->cities as $city1){
            if (!$this->hasCity($city1)) {
                $lastVisited = empty($result) ? null : end($result);
                $result[] = $city1;
                $cost += is_null($lastVisited) ? 0 : $lastVisited->getDistance($city1);
            }
        }

        return new Path($result, $cost);
    }

    /** @return int */
    public function count() {
        return count($this->cities);
    }

    /** @return array */
    public function getDistanceMatrix() {
        $distances = [];
        for ($i=0 ; $i<$this->count(); $i++) {
            for ($j=0 ; $j<$this->count(); $j++) {
                $cityI = $this->getCity($i);
                $cityJ = $this->getCity($j);
                $distance = $cityI->getDistance($cityJ);
                $distances[$i][$j] = [(string)$distance => $cityJ];
            }
        }

        $this->sortDistanceMatrix($distances);
        return $distances;
    }

    /**
     * @param array $distances
     */
    protected function sortDistanceMatrix(&$distances) {
        $size = count($distances[0]);

        for ($k=0; $k<$size; $k++) {
            for ($i=0; $i<$size; $i++) {
                for ($j=$i+1; $j<$size; $j++) {
                    if (floatval(key($distances[$k][$i])) > floatval(key($distances[$k][$j]))) {
                        $tmp = $distances[$k][$i];
                        $distances[$k][$i] = $distances[$k][$j];
                        $distances[$k][$j] = $tmp;
                    }
                }
            }
        }
    }

    /** @return float */
    protected function getTotalDistance() {
        /** @var City $cityTmp */
        $cityTmp = null;
        $distance = 0;

        foreach ($this->cities as $city) {
            $distance += is_null($cityTmp) ? 0 : $cityTmp->getDistance($city);
            $cityTmp = $city;
        }

        return $distance;
    }
}
