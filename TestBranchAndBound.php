<?php

class TestBranchAndBound extends Test
{
    public function testGetMinPath() {
        $cities = new Path([
            new City(1,'City 1', 40.21, 40.38),
            new City(2, 'City 2', 10.24, 10.35),
            new City(3, 'City 3', 20.23, 20.36),
            new City(4, 'City 4', 30.22, 30.37)

        ]);

        $expectedResult = new Path([
            new City(1, 'City 1', 40.21, 40.38),
            new City(4, 'City 4', 30.22, 30.37),
            new City(3, 'City 3', 20.23, 20.36),
            new City(2, 'City 2', 10.24, 10.35)
        ]);

        $algorithm = new BranchAndBound($cities);
        $result = $algorithm->getMinPath();

        $this->assertSameCities($result, $expectedResult);
    }
}
