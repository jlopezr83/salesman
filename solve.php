<?php

include_once 'City.php';
include_once 'Path.php';
include_once 'Candidates.php';
include_once 'BranchAndBound.php';
include_once 'utils.php';

const MAX_MINUTES = 15;

$cities = readCities('cities.txt');
$algorithm = new BranchAndBound($cities, MAX_MINUTES);
$minPath = $algorithm->getMinPath();

foreach ($minPath->cities as $city) {
    echo $city->name . "\n";
}
