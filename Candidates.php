<?php


class Candidates
{
    /** @var Path[] */
    public $paths;

    /**
     * Candidates constructor.
     * @param Path[] $paths
     */
    public function __construct(array $paths = [])
    {
        $this->paths = $paths;
    }

    /**
     * @return int
     */
    public function count(){
        return count($this->paths);
    }

    /**
     * @return Path smallest optimalCost
     */
    public function next(){
        return array_shift($this->paths);
    }

    public function insert($candidate){
        $this->insertBinary($this->paths, $candidate, 0, $this->count()-1);
    }

    /**
     * Recursive algorithm to sort the candidates by optimalCost
     * @param Path[] $candidates
     * @param Path $candidate
     * @param int $start
     * @param int $end
     */
    protected function insertBinary(&$candidates, $candidate, $start, $end ) {
        $med = $start + floor(($end-$start)/2);

        if (empty($candidates)) {
            $candidates[]= $candidate;
            return;
        }

        if (($candidate->optimalCost > $candidates[$end]->optimalCost)) {
            array_splice($candidates, $end+1, 0, [$candidate]);
            return;
        }

        if ($candidate->optimalCost <= $candidates[$start]->optimalCost) {
            array_splice($candidates, $start, 0, [$candidate]);
            return;
        }

        if ($candidate->optimalCost < $candidates[$med]->optimalCost) {
            $this->insertBinary($candidates, $candidate, $start, $med-1);
            return;
        }

        if ($candidate->optimalCost > $candidates[$med]->optimalCost) {
            $this->insertBinary($candidates, $candidate, $med+1, $end);
            return;
        }
    }
}
