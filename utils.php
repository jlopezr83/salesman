<?php

/**
 * @param string $path
 * @return Path
 */
function readCities($path) {
    $fp = fopen($path, 'r');
    $cities = [];
    $i=1;
    while ($city = fgets($fp, 2048)) {
        $city = explode("\t", trim($city));
        if (count($city) != 3) {
            echo "Problem reading city " . join('', $city);
            continue;
        }
        $cities[] = new City($i, $city[0], floatval($city[1]), floatval($city[2]));
        $i++;
    }

    return new Path($cities);
}

