<?php

class TestCandidates extends Test
{
    public function testCount(){
        $cities1 = new Path([
            new City(1,'City 1', 50.21, 50.38),
            new City(2, 'City 2', 0.24, 0.35)
        ], null, 20.2);

        $cities2 = new Path([
            new City(3, 'City 3', 20.23, 20.36),
            new City(4, 'City 4', 11.22, 11.37)
        ], null, 45.7);

        $candidate = new Candidates([$cities1, $cities2]);
        assert('$candidate->count() === 2', 'Invalid count');
    }

    public function testInsert() {
        $cities1 = new Path([
            new City(1,'City 1', 50.21, 50.38),
            new City(2, 'City 2', 0.24, 0.35)
        ], null, 20.2);

        $cities2 = new Path([
            new City(3, 'City 3', 20.23, 20.36),
            new City(4, 'City 4', 11.22, 11.37)
        ], null, 45.7);

        $candidate1 = $cities1;
        $candidates = new Candidates([$candidate1]);

        $candidate2 = $cities2;
        $candidates->insert($candidate2);

        $candidate3 = new Path($cities1->cities, $cities1->cost, $cities1->optimalCost);
        $candidate3->addCities($cities2);
        $candidates->insert($candidate3);

        $candidate4 = new Path([new City(1,'City 1', 50.21, 50.38)], null, 76.9);
        $candidates->insert($candidate4);

        $expectedResult = new Candidates([$candidate3, $candidate1, $candidate2, $candidate4]);

        for($i=0; $i<$expectedResult->count();$i++){
            assert('$candidates->candidates[$i]->cost == $expectedResult->candidates[$i]->cost', "Invalid cost");
            $this->assertSameCities($candidates->paths[$i], $expectedResult->paths[$i]);
        }
    }

    public function testNext(){
        $cities1 = new Path([
            new City(1,'City 1', 50.21, 50.38),
            new City(2, 'City 2', 0.24, 0.35)
        ], null, 20.2);

        $cities2 = new Path([
            new City(3, 'City 3', 20.23, 20.36),
            new City(4, 'City 4', 11.22, 11.37)
        ], null, 45.7);

        $candidates = new Candidates([$cities2]);
        $candidates->insert($cities1);

        $this->assertSameCities($cities1, $candidates->next());
    }
}
