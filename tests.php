<?php

include_once 'City.php';
include_once 'Path.php';
include_once 'Candidates.php';
include_once 'BranchAndBound.php';

include_once 'Test.php';
include_once 'TestCity.php';
include_once 'TestPath.php';
include_once 'TestCandidates.php';
include_once 'TestBranchAndBound.php';


$testCity = new TestCity();
$testCity->run();

$testPath = new TestPath();
$testPath->run();

$testCandidates = new TestCandidates();
$testCandidates->run();

$testBranchAndBound = new TestBranchAndBound();
$testBranchAndBound->run();
