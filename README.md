# Salesman Problem

## Problem definition 
Find the shortest path in a list of cities starting always from one city (Beijing).
For n cities we have (n-1)! combinations because we know the first city.
Having 32 cities we have (32-1)! combinations. It's impossible to check every
combination, for that I'm applying a Branch & Bound algorithm. 
Using this algorithm I'm not checking the combinations that have more cost than 
the best cost I checked until the moment.


## Run script
```
php solve.php
```

## Run tests
```
php tests.php
```
