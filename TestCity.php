<?php

class TestCity extends Test
{
    public function testGetDistance(){
        $city1 = new City(1,'City 1', 20.0, 20.0);
        $city2 = new City(2, 'City 2', 0.0, 0.0);
        assert('$city1->getDistance($city2) == 28.28', 'Invalid distance');
    }
}
